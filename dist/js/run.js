function Menu(options) {
    let elem = options.elem;
    let mainMenu = options.mainMenu;
    let doings = options.doings;
    let news = options.news;
    let end= options.end;
    let start= options.start;
    let img;

    $(elem).on("click",  (e)=> {
        img = e.target.children[0];
        if (elem.classList.contains("next")) {
            $(img).attr("src", elem.dataset.linkPrev);
            $(elem).toggleClass("next prev");
            $(elem).css({"background":'#0187d0'});
            $(mainMenu).animate({
                left: "0px"
            }, 1000)
        }else{
            $(img).attr("src", elem.dataset.linkNext);
            $(elem).toggleClass("next prev");
            $(elem).css({"background":'gray'});
            $(mainMenu).animate({
                left: "-250px"
            }, 1000)
        }
    });
    $(end).on("click", () =>{
        $(doings).css({"display":'none'});
        $(news).css({"display":'block'});
        $(mainMenu).animate({
            left : "-250px"
        },1000);
        $(img).attr("src", elem.dataset.linkNext);
        $(elem).toggleClass("next prev");
        $(elem).css({"background":'gray'});
    });
    $(start).on("click", () =>{
        $(news).css({"display":'none'});
        $(doings).css({"display":'block'});
        $(mainMenu).animate({
            left : "-250px"
        },1000);
        $(img).attr("src", elem.dataset.linkNext);
        $(elem).toggleClass("next prev");
        $(elem).css({"background":'gray'});
    })
}


let menu = new Menu({
    elem: document.querySelector('.direction'),
    mainMenu: document.querySelector(".main-menu"),
    doings : document.querySelector(".doings"),
    news : document.querySelector(".news"),
    end: document.querySelector(".end"),
    start: document.querySelector(".start")
});


let fun5= () =>{
	$('#upload').trigger('click');
};
function fun6(e){
 $(".active").removeClass("active");
 $(this).addClass("active");
 if(this.innerText=="История загрузок"){
 	$(".table").css({"display":'block'});
 	$(".forma").css({"display":'none'});
 }else{
 	$(".table").css({"display":'none'});
 	$(".forma").css({"display":'block'});
 }

};
let tab = (arr)=>{
	let tbody = document.querySelector("tbody");
	if(tbody.children.length!=0){

		for (var z = 0; z < tbody.children.length; z++) {
			tbody.removeChild(tbody.children[z]);
			z--;

		}
	}
	for(var i =0; i<arr.length; i++){
		var tr = document.createElement('tr');
			for(var key in arr[i]){
				var td = document.createElement('td');
				td.innerText = arr[i][key];
				tr.appendChild(td);
				}

	$("tbody").append(tr)
	}



};

let rest = (obj)=>{
	if(JSON.parse(localStorage.getItem("arr")) !=null){
		var newArr = JSON.parse(localStorage.getItem("arr"));
		console.log(newArr)
		newArr.push(obj);
		localStorage.setItem('arr', JSON.stringify(newArr))
		tab(newArr)
	}else{
		var newArr = [];
		newArr.push(obj)
		localStorage.setItem('arr', JSON.stringify(newArr))

		tab(newArr)
			}
}

function FilterGroup(options) {
    let filterGroup = options.filterGroup;
    let rowTd = options.rowTd;
    let arr =[];
    $(filterGroup).on("click", (e)=>{
        let img = filterGroup.children[0];
        if (filterGroup.classList.contains("up")) {
            for (let i = rowTd.children.length - 1; i >= 0; i--) {
                let child = rowTd.removeChild(rowTd.children[i]);
                arr.push(child);
            }
            arr.sort((a, b) => {
                if (a.firstChild.innerHTML > b.firstChild.innerHTML) return 1;
                if (a.firstChild.innerHTML < b.firstChild.innerHTML) return -1;
            });
            for (let i = 0; i < arr.length; i++) {
                rowTd.appendChild(arr[i]);
            }
            arr = [];
            $(filterGroup).toggleClass("up down");
            $(img).attr("src", filterGroup.dataset.linkUp);
        }else {
            for (let i = rowTd.children.length - 1; i >= 0; i--) {
                let child = rowTd.removeChild(rowTd.children[i]);
                arr.push(child);
            }
            arr.sort((a, b) => {
                if (a.firstChild.innerHTML < b.firstChild.innerHTML) return 1;
                if (a.firstChild.innerHTML > b.firstChild.innerHTML) return -1;
            });
            for (let i = 0; i < arr.length; i++) {
                rowTd.appendChild(arr[i]);

            }
            arr = [];
            $(filterGroup).toggleClass("up down");
            $(img).attr("src", filterGroup.dataset.linkDown);
        }
    })

};
let filterGroup = new FilterGroup({
    filterGroup: document.querySelector('.filterGroup'),
    rowTd: document.getElementsByTagName("tbody")[0]
});


(function($){
		$("#nexFile").on("click",fun5)
		$(".nameFile").on("click",fun5)
		$("#upload").change(function(e){
			var filename = $(this).val().replace(/.*\\/, "");
			$(".nameFile").val(filename);
		})
		$(".oborot").on("click",'button',fun6);

		$(".submit").on("click",function(e){

			let obj = {};
			obj.group = $(".sel1").val();
			obj.stud = $(".sel2").val();
			obj.text = $("textarea").val();
			obj.file = $(".nameFile").val();


			rest(obj)
		});
		$(document).ready(function(){
  			let def = JSON.parse(localStorage.getItem("arr"));
  				for(var i =0; i<def.length; i++){
		var tr = document.createElement('tr');
			for(var key in def[i]){
				var td = document.createElement('td');
				td.innerText = def[i][key];
				tr.appendChild(td)
				}

	$("tbody").append(tr)
	}
});


$.ajax({
  type: "GET",
  url: "js/start.js",
  dataType: "script",
  success: function(){
  var fly = JSON.parse(obj);

 	var newCall = document.createElement("div");
 	newCall.innerText = fly.col1;
 	newCall.className = "col1-1";
 	$(".col1").append(newCall);

 	var newCall2 = document.createElement("div");
 	newCall2.innerText = fly.col2;
 	newCall2.className = "col2-2";
 	$(".col2").append(newCall2);

 	$(".end").text(fly.end);
 	$(".start").text(fly.start);
 	$(".but1").text(fly.but1);
 	$(".but2").text(fly.but2);



    }
});
})(jQuery);