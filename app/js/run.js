(function($){
    function Menu(options) {
        let elem = options.elem;
        let mainMenu = options.mainMenu;
        let doings = options.doings;
        let news = options.news;
        let end= options.end;
        let start= options.start;
        let img;

        $(elem).on("click",  (e)=> {
            img = e.target.children[0];
             if (elem.hasClass("next")) { //разварачиваем меню
                $(img).attr("src", elem.data('linkPrev'));
                $(elem).toggleClass("next prev");
                $(elem).css({"background":'#0187d0'});
                $(mainMenu).animate({
                    left: "0px"
                }, 1000)
             }else{ //сворачиваем меню
                $(img).attr("src", elem.data('linkNext'));
                $(elem).toggleClass("next prev");
                $(elem).css({"background":'gray'});
                $(mainMenu).animate({
                    left: "-250px"
                }, 1000)
             }
        });
        $(end).on("click", () =>{ //меняем отображение страницы на новости
            $(doings).css({"display":'none'});
            $(news).css({"display":'block'});
            $(mainMenu).animate({
                left : "-250px"
            },1000);
            $(img).attr("src", elem.data('linkNext'));
            $(elem).toggleClass("next prev");
            $(elem).css({"background":'gray'});
        });
        $(start).on("click", () =>{ //меняем отображение страницы на загрузки
            $(news).css({"display":'none'});
            $(doings).css({"display":'block'});
            $(mainMenu).animate({
                left : "-250px"
            },1000);
            $(img).attr("src", elem.data('linkNext'));
            $(elem).toggleClass("next prev");
            $(elem).css({"background":'gray'});
        })
    }


    let menu = new Menu({
        elem: $('.direction'),
        mainMenu: $(".main-menu"),
        doings : $(".doings"),
        news : $(".news"),
        end: $(".end"),
        start: $(".start")
    });


    let addFile= () =>{
	    $('#upload').trigger('click');
    };
    function changeTabs(e){
        $(".active").removeClass("active");
        $(this).addClass("active");
        if($(this).text()=="История загрузок"){
 	        $(".table").css({"display":'block'});
 	        $(".forma").css({"display":'none'});
        }else{
 	        $(".table").css({"display":'none'});
 	        $(".forma").css({"display":'block'});
        }
    };
    let tab = (arr)=>{
	    let tbody = $("tbody");
	    if(tbody.children().length!=0){
			    tbody.find(tbody.children()).remove();
	    };
	    for(let i =0; i<arr.length; i++){
            tbody.append("<tr>");
            let z = 0;
			    for(let key in arr[i]){
                    $("tbody tr:eq("+i+")").append("<td>");
                    $("tbody tr:eq("+i+") td:eq("+z+")").text(arr[i][key]);
                    z++;
				}

            }
    };

    let rest = (obj)=>{ //записываем данные в localStorage
	    if(JSON.parse(localStorage.getItem("arr")) !=null){
		    let newArr = JSON.parse(localStorage.getItem("arr"));

		    newArr.push(obj);
		    localStorage.setItem('arr', JSON.stringify(newArr))
		    tab(newArr)
	    }else{
		    let newArr = [];
	 	    newArr.push(obj)
		    localStorage.setItem('arr', JSON.stringify(newArr))
		    tab(newArr)
			}
    }


    function FilterGroup(options) { //функция фильтрации по группам
        let filterGroup = options.filterGroup;
        let rowTd = options.rowTd;
        let arr =[];
        $(filterGroup).on("click", (e)=>{
            let img = filterGroup.children[0];
            if (filterGroup.hasClass("up")) {
                for (let i = rowTd.children.length - 1; i >= 0; i--) {
                    let child = rowTd.children[i];
                    rowTd.children[i].remove()
                    arr.push(child);
                }
                arr.sort((a, b) => {
                    if (a.firstChild.innerHTML > b.firstChild.innerHTML) return 1;
                    if (a.firstChild.innerHTML < b.firstChild.innerHTML) return -1;
                });
                for (let i = 0; i < arr.length; i++) {
                    rowTd.append(arr[i]);
                }
                arr = [];
                $(filterGroup).toggleClass("up down");
                $(img).attr("src", filterGroup.data('linkUp'));
            }else {
                for (let i = rowTd.children.length - 1; i >= 0; i--) {
                    let child = rowTd.children[i];
                    rowTd.children[i].remove()
                    arr.push(child);
                }
                arr.sort((a, b) => {
                    if (a.firstChild.innerHTML < b.firstChild.innerHTML) return 1;
                    if (a.firstChild.innerHTML > b.firstChild.innerHTML) return -1;
                });
                for (let i = 0; i < arr.length; i++) {
                    rowTd.append(arr[i]);

                }
                arr = [];
                $(filterGroup).toggleClass("up down");
                $(img).attr("src", filterGroup.data('linkDown'));
            }
        })

    }

    let filterGroup = new FilterGroup({
        filterGroup: $('.filterGroup'),
        rowTd: $("tbody")[0]
    });

    $("#nexFile").on("click",addFile)
    $(".nameFile").on("click",addFile)
    $("#upload").change(function(e){
        let filename = $(this).val().replace(/.*\\/, "");
        $(".nameFile").val(filename);
    })
    $(".oborot").on("click",'button',changeTabs);
    $(".submit").on("click",function(e){
        let obj = {};
        obj.group = $(".sel1").val();
        obj.stud = $(".sel2").val();
        obj.text = $("textarea").val();
        obj.file = $(".nameFile").val();
        rest(obj)
    });

    $(document).ready(()=>{ //запись в таблицу при загрузке
        let def = JSON.parse(localStorage.getItem("arr"));
        for(let i =0; i<def.length; i++){
            $('tbody').append("<tr>");
            let z =0;
			for(let key in def[i]){
                $("tbody tr:eq("+i+")").append("<td>");
                $("tbody tr:eq("+i+") td:eq("+z+")").text(def[i][key]);
                z++;
            }
	    }
    });


    $.ajax({ //аякс запрос
        type: "GET",
        url: "js/start.js",
        dataType: "script",
        success: ()=>{
            let fly = JSON.parse(obj);

            $(".col1").append('<div>');
            $(".col1 div").addClass("col1-1").text(fly.col1);

            $(".col2").append('<div>');
            $(".col2 div").addClass("col2-2").text(fly.col2);

 	        $(".end").text(fly.end);
 	        $(".start").text(fly.start);

 	        $(".but1").text(fly.but1);
 	        $(".but2").text(fly.but2);
        }
    });
})(jQuery);